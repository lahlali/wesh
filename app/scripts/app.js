'use strict';

/**
 * @ngdoc overview
 * @name weshApp
 * @description
 * # weshApp
 *
 * Main module of the application.
 */
angular
  .module('weshApp', [
    'ngAnimate',
    'ngRoute',
    'ngTouch',
    'firebase'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/user', {
        templateUrl: 'views/user.html',
        controller: 'UserCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
