'use strict';

/**
 * @ngdoc function
 * @name weshApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the weshApp
 */
angular.module('weshApp')
  .controller('AboutCtrl', function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  });
