'use strict';

/**
 * @ngdoc function
 * @name weshApp.controller:AboutCtrl
 * @description
 * # AboutCtrl
 * Controller of the weshApp
 */
angular.module('weshApp')
  .controller('UserCtrl', function ($rootScope,$scope) {
    $scope.user=$rootScope.user;

  });
